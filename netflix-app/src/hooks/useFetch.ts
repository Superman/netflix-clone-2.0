import { useState, useEffect } from 'react';

type FetchStatus = 'idle' | 'loading' | 'success' | 'error';

interface FetchResult<T> {
  data: T | null;
  status: FetchStatus;
  error: string | null;
}

interface FetchOptions {
  headers?: HeadersInit;
  method?: string;
  body?: BodyInit;
  // Add more options as needed
}

function useFetch<T>(url: string, options?: FetchOptions): FetchResult<T> {
  const [data, setData] = useState<T | null>(null);
  const [status, setStatus] = useState<FetchStatus>('idle');
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    let isMounted = true;

    const fetchData = async () => {
      try {
        setStatus('loading');
        const response = await fetch(url, options);
        const json = await response.json();
        
        if (isMounted) { // Check if the component is still mounted
          setData(json);
          setStatus('success');
        }
      } catch (error: any) {
        if (isMounted) { // Check if the component is still mounted
          setError(error.message);
          setStatus('error');
        }
      }
    };

    fetchData();

    // Clean up the effect if the component is unmounted
    return () => {
      isMounted = false;
    };
  }, [url, options]);

  return { data, status, error };
}

export default useFetch;
