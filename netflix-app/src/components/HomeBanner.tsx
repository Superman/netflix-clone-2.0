//react imports
import { requests, options } from "../utils/requests";

//custom hook
import useFetch from '../hooks/useFetch';

interface MyData {
    // Define the structure of your JSON data here
    results: any;
    backdrop_path: string;
    original_title: string;
    title: string;
    overview: string;
}

export default function HomeBanner() {

    const url = 'https://api.themoviedb.org/3' + requests.fetchMovies;

    const { data, status, error } = useFetch<MyData | null>(url, options);

    const truncate = (text: string | undefined, maxLength: number): string => {
        if (!text) return ''; // Handle the case when text is undefined
        if (text.length <= maxLength) return text;
        return text.substring(0, maxLength) + '...';
    };

    if (status === 'loading') {
        return <div>Loading...</div>;
    }

    if (status === 'error') {
        return <div>Error: {error}</div>;
    }

    if (status === 'success') {

        const movie = data?.results[Math.floor(Math.random() * data?.results.length - 1)]

        return (
            <div className="home-banner relative">

                <img className="object-cover w-full min-h-[500px]" src={`https://image.tmdb.org/t/p/original/${movie.backdrop_path}`} alt="banner" />

                <div className="home-banner__contents absolute top-0 left-0 w-full h-full flex items-center px-10">
                    <div className="home-banner__contents-wrapper text-white flex flex-col gap-2">
                        <div className="home-banner__title text-3xl uppercase">
                            {movie.original_title || movie.title}
                        </div>
                        <div className="banner__buttons text-xs flex gap-1">
                            <div className="bg-[#111] py-1 px-3">Play</div>
                            <div className="bg-[#111] py-1 px-3">My List</div>
                        </div>
                        <div className="home-banner__desc text-lg max-w-md leading-snug">
                            {truncate(movie.overview, 160)}
                        </div>
                    </div>
                </div>

                <div className="home-banner__fade-bottom w-full h-[35%] absolute bottom-0 left-0 bg-gradient-to-b from-transparent via-rgba(37, 37, 37, 0.6) to-[#111]"></div>

            </div>
        )
    }

}




