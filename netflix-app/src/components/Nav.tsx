
//react imports
import { useEffect, useState } from 'react';

//Assets
import { UserCircleIcon } from '@heroicons/react/24/solid'
import logo from '../assets/netflix-logo.png';

export default function Nav() {

    const [show, setShow] = useState(false);

    const transitionNavBar = () => {
        if (window.scrollY > 50)
            setShow(true);
        else
            setShow(false);
    }

    useEffect(() => {
        window.addEventListener("scroll", transitionNavBar);
        return () => window.removeEventListener("scroll", transitionNavBar);
    }, []);

    return (
        <div className={`nav fixed top-0 left-0 w-full transition-colors duration-500 ease-in-out z-10 ${show ? 'bg-black' : ''}`}>
            <div className="nav-container p-6 flex w-full items-center justify-between">
                <div className="nav__logo cursor-pointer">
                    <img className="w-28" src={logo} alt="logo" />
                </div>
                <div className="nav__avatar cursor-pointer">
                    <UserCircleIcon className="w-10 text-red-800" />
                </div>
            </div>
        </div>
    )
}
