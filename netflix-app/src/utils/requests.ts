const API_KEY = import.meta.env.API_READ_ACCESS_TOKEN;

export const options = {
    method: 'GET',
    headers: {
        accept: 'application/json',
        Authorization: `Bearer ${API_KEY}`,
    },
};

export const requests = {
    fetchMovies: `/discover/movie?include_adult=false&include_video=false&language=en-US&page=1&sort_by=popularity.desc`
};
